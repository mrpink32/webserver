#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fenv.h>

typedef struct
{
    int sockfd;
    u_int16_t port;
    struct sockaddr_in addr;
    struct sockaddr_in client_addr;
} webserver;

// webserver *webserver_create(int32_t sockfd, u_int16_t port)
// {
//     webserver *ws = malloc((sizeof(webserver)));
//         ws->sockfd = sockfd;
//     ws->port = port;
//     return ws;
// }

void test(webserver *ws, const char *text)
{
    printf("%s\n", text);
}

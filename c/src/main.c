#include "webserver.h"

const u_int16_t PORT = 8080;
const char SRC_DIR[] = "/mnt/DATA/Repositories/RobotteknikA/Eksamen/data";

enum
{
    BAD_REQUEST = -1,
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH,
};

typedef struct
{
    char *key;
    int32_t value;
} key_value_pair;

static key_value_pair request_lookup_table[] = {
    {"GET", GET},
    {"HEAD", HEAD},
    {"POST", POST},
    {"PUT", PUT},
    {"DELETE", DELETE},
    {"CONNECT", CONNECT},
    {"OPTIONS", OPTIONS},
    {"TRACE", TRACE},
    {"PATCH", PATCH},
};

int32_t key_from_string(char *request)
{
    for (size_t i = 0; i < sizeof(request_lookup_table) / sizeof(key_value_pair); i++)
    {
        key_value_pair *pair = &request_lookup_table[i];
        if (strcmp(pair->key, request) == 0)
        {
            return pair->value;
        }
    }
    return BAD_REQUEST;
}

int32_t send_file(int32_t client_sockfd, char *path, char *read_mode)
{
    // open file
    printf("path: %s\n", path);
    printf("opening file...\n");
    FILE *file = fopen(path, read_mode);
    if (file == NULL)
    {
        perror("fopen");
        return -1;
    }
    printf("file opened\n");

    // send file
    printf("sending file...\n");
    fseek(file, 0, SEEK_END);        // seek to end of file
    int32_t file_size = ftell(file); // get current file pointer
    fseek(file, 0, SEEK_SET);        // seek back to beginning of file
    char file_buf[file_size];
    int32_t bytes_read = 0;
    while ((bytes_read = fread(file_buf, 1, sizeof(file_buf), file)) > 0)
    {
        send(client_sockfd, file_buf, bytes_read, 0);
    }
    printf("file sent\n");
    fclose(file);
    return 0;
}

int main(int argc, char *argv[])
{
    // create a socket
    int32_t sockfd = 0;
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int32_t){1}, sizeof(int32_t)) < 0)
    {
        perror("setsockopt");
        exit(1);
    }

    // bind socket to the port defined as PORT
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(1);
    }

    // listen for incoming connections
    if (listen(sockfd, 5) < 0)
    {
        perror("listen");
        exit(1);
    }

    // accept incoming connections
    struct sockaddr_in client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    int32_t client_sockfd = 0;
    FILE *file;
    while (1)
    {
        if ((client_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &client_addr_len)) < 0)
        {
            perror("accept");
            continue;
        }
        printf("Client IP: %d\n", client_addr.sin_addr.s_addr);
        printf("Client Port: %d\n", client_addr.sin_port);

        // receive request
        constexpr int32_t BUFFER_SIZE = 1024;
        char packet[1024];
        int32_t bytes_read = 0;
        if ((bytes_read = recv(client_sockfd, packet, sizeof(packet), 0)) < 0)
        {
            perror("recv");
            continue;
        }
        printf("request:\n%s", packet);

        int32_t request_length = strlen(packet) - strlen(strstr(packet, "\r\n"));
        char *request = strtok(packet, "\r\n");
        // char request[request_length];
        // strncpy(request, packet, request_length);
        printf("request is %ld bytes long and contains: %s\n", sizeof(request), request);

        // parse request
        char *token = strtok(request, " ");
        token = strtok(NULL, " ");
        printf("request: %s\n", request);
        printf("requested item: %s\n", token);

        switch (key_from_string(request))
        {
        case GET:
        {
            if (strcmp(token, "/") == 0)
            {
                token = "/index.html";
            }
            char path[strlen(token) + strlen(SRC_DIR)];
            if (sprintf(path, "%s%s", SRC_DIR, token) < 0)
            {
                perror("sprintf");
                continue;
            }

            // mime parser
            char mime_type[100];
            char *type = strtok(strstr(token, "."), ".");
            if (type == NULL)
            {
                // *mime_type = "text/plain";
                sprintf(mime_type, "text/plain");
            }
            else if (strcmp(type, "js") == 0)
            {
                // *mime_type = "text/javascript";
                sprintf(mime_type, "text/javascript");
            }
            else if (strcmp(type, "ico") == 0)
            {
                // *mime_type = "image/x-icon";
                sprintf(mime_type, "image/x-%sn", type);
            }
            else
            {
                sprintf(mime_type, "text/%s", type);
            }
            printf("type: %s\n", type);

            // send headers
            char headers[100];
            sprintf(headers, "HTTP/1.1 200 OK\r\nContent-Type: %s\r\n\r\n", mime_type);
            send(client_sockfd, headers, strlen(headers), 0);

            char *read_mode = "r";
            if (mime_type == "image/x-icon")
            {
                read_mode = "rb";
            }

            if (send_file(client_sockfd, path, read_mode) < 0)
            {
                perror("send_file");
                continue;
            }
            break;
        }
        case POST:
            printf("POST test!!\n");
            break;
        case BAD_REQUEST:
            printf("BAD_REQUEST test!!\n");
            break;
        default:
            printf("default test!!\n");
            break;
        }

        // if (strcmp(request, "GET") == 0)
        // {
        //         }

        // close socket
        close(client_sockfd);
    }
    return 0;
}

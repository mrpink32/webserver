with import <nixpkgs> {};

stdenv.mkDerivation {
    nativeBulidInputs = [
        llvmPackages_18.stdenv
        clang_18
        clang-tools_18
        pkg-config
    ];

    buildInputs = [
        llvmPackages_18.stdenv
        clang-tools_18
        glibc.dev
    ];
    
    pname = "main";
    version = "0.0.1";
    
    src = ./src;
    
    buildPhase = ''
        cc -std=c2x main.c
    '';
    
    installPhase = ''
        mkdir -p $out/bin
        mv a.out $out/bin/"$pname-$version"
    '';
}

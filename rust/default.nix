with import <nixpkgs> {};

mkShell {
    nativeBuildInputs = [
        cargo
        rustc
        rustfmt
        clippy
    ];

    RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";

    buildInputs = [
        glibc.dev
    ];

}

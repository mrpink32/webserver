use std::{
    fs::{File, OpenOptions},
    io::{BufWriter, Write},
    path::Path,
    sync::Arc,
};
use crate::datetime;

pub struct Logger {
    file: File,
}

impl Logger {
    pub fn new(path: &'static str) -> Arc<Logger> {
        let file_location: &Path = Path::new(path);
        let file: File = OpenOptions::new()
            .append(true)
            .create(true)
            .write(true)
            .open(file_location)
            .expect("Failed to open file!");
        Arc::new(Logger { file })
    }

    pub fn log(&self, text: &str) {
        let sys_time: String = datetime::get_time();
        let text_result: String = format!("[{}]LOG:\n{}\n", sys_time, text);
        let mut buffered_writer =
            BufWriter::new(self.file.try_clone().expect("Failed to clone file!"));
        drop(
            buffered_writer
                .write_all(text_result.as_bytes())
                .expect("Unable to write data"),
        );
    }

    pub fn log_error(&self, text: &str) {
        let sys_time: String = datetime::get_time();
        let text_result: String = format!("[{}]ERROR:\n{}\n", sys_time, text);
        let mut buffered_writer =
            BufWriter::new(self.file.try_clone().expect("Failed to clone file!"));
        let _ = buffered_writer
            .write_all(text_result.as_bytes())
            .expect("Unable to write data");
    }
}
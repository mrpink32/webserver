mod datetime;
pub mod logging;

use std::{
    collections::HashMap,
    env::args,
    fs,
    fs::{File, OpenOptions},
    io::{BufRead, BufReader, Read, Write},
    net::{Ipv4Addr, TcpListener, TcpStream},
    path::Path,
    str::FromStr,
    sync::{mpsc, Arc, Mutex},
    thread::{spawn, JoinHandle},
};

use crate::logging::*;

const DEFAULT_PORT: u16 = 9000;
const LOG_PATH: &str = "webserver.log";
const CONFIG_PATH: &str = "config.conf";

pub struct WebServer {
    ip: Ipv4Addr,
    port: u16,
}

impl Default for WebServer {
    fn default() -> Self {
        Self {
            ip: Ipv4Addr::new(127, 0, 0, 1),
            port: 80,
        }
    }
}

impl WebServer {
    pub fn new<A>(ip: A, port: u16) -> Self
    where
        A: Into<Ipv4Addr>,
    {
        Self {
            ip: ip.into(),
            port,
        }
    }

    pub fn builder() -> WebServerBuilder {
        WebServerBuilder::default()
    }
}

pub struct WebServerBuilder {
    ip: Ipv4Addr,
    port: u16,
}

impl Default for WebServerBuilder {
    fn default() -> Self {
        Self {
            ip: Ipv4Addr::new(127, 0, 0, 1),
            port: 80,
        }
    }
}

impl WebServerBuilder {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn ip<A>(mut self, ip: A) -> Self
    where
        A: Into<Ipv4Addr>,
    {
        self.ip = ip.into();
        self
    }

    pub fn port(mut self, port: u16) -> Self {
        self.port = port;
        self
    }

    pub fn build(self) -> WebServer {
        WebServer {
            ip: self.ip,
            port: self.port,
        }
    }
}

#[derive(Default)]
pub struct Response {
    status: u32,
    headers: HashMap<String, String>,
    body: Vec<u8>,
}

impl Response {
    pub fn builder() -> ResponseBuilder {
        ResponseBuilder::default()
    }
    pub fn write(self, mut stream: TcpStream) {
        stream.write_all(&self.body).expect("Failed to write body!");
        // stream.write_all(&self.body).expect("Failed to write body!");
        stream.flush().expect("Failed to flush IO stream!");
    }
}

#[derive(Default)]
pub struct ResponseBuilder {
    status: u32,
    body: Vec<u8>,
}

impl ResponseBuilder {
    pub fn new() -> Self {
        Self {
            status: 0,
            body: Vec::new(),
        }
    }
    pub fn status(mut self, status: u32) -> Self {
        let status_line = match status {
            404 => String::from("HTTP/1.1 404 NOT FOUND\r\n"),
            200 => String::from("HTTP/1.1 200 OK\r\n"),
            _ => String::from("HTTP/1.1 404 NOT FOUND\r\n"),
        };
        // self.body.push_str(&status_line);
        self.body.append(&mut status_line.as_bytes().to_vec());
        self
    }
    pub fn length(mut self, length: i32) -> Self {
        let content_length: String = format!("Content-Length: {}", length);
        // self.body.push_str(&content_length);
        self.body.append(&mut content_length.as_bytes().to_vec());
        self
    }
    pub fn content(mut self, filepath: &Path) -> Self {
        let mut contents = match filepath.extension().unwrap().to_str() {
            Some("png") => fs::read(filepath).expect("Failed to read png!"),
            Some("ico") => fs::read(filepath).expect("Failed to read ico!"),
            // Some("json") => CPUData::get_server_data(),
            _ => fs::read(filepath).expect("Failed to read default!!"),
        };
        // let response = format!("\r\n{}", contents);
        let response: String = String::from("\r\n\r\n");
        // self.body.push_str(&response);
        self.body.append(&mut response.as_bytes().to_vec());
        self.body.append(&mut contents);
        self
    }
    pub fn build(self) -> Response {
        Response {
            status: self.status,
            body: self.body,
            ..Default::default()
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Method {
    Get,
    Head,
    Post,
    Put,
    Delete,
    Connect,
    Options,
    Trace,
    Patch,
    BadRequest,
}

impl Method {
    pub fn from(string: &str) -> Method {
        return match string.to_uppercase().as_bytes() {
            b"GET" => Method::Get,
            b"HEAD" => Method::Head,
            b"POST" => Method::Post,
            b"PUT" => Method::Put,
            b"DELETE" => Method::Delete,
            b"CONNECT" => Method::Connect,
            b"OPTIONS" => Method::Options,
            b"TRACE" => Method::Trace,
            b"PATCH" => Method::Patch,
            _ => {
                println!("Unknown method: {}", string);
                Method::BadRequest
            }
        };
    }
    pub fn to_string(&self) -> String {
        return match self {
            Method::Get => "GET".to_string(),
            Method::Head => "HEAD".to_string(),
            Method::Post => "POST".to_string(),
            Method::Put => "PUT".to_string(),
            Method::Delete => "DELETE".to_string(),
            Method::Connect => "CONNECT".to_string(),
            Method::Options => "OPTIONS".to_string(),
            Method::Trace => "TRACE".to_string(),
            Method::Patch => "PATCH".to_string(),
            Method::BadRequest => "BAD_REQUEST".to_string(),
        };
    }
}

#[derive(Debug)]
pub struct Request {
    method: Method,
    url: String,
    headers: String,
    body: Vec<u8>,
}

impl Request {
    pub fn new(test: &String) -> Request {
        let http_command = test.split_whitespace().collect::<Vec<&str>>();
        Request {
            method: Method::from(http_command[0]),
            url: http_command[1].to_string(),
            headers: http_command[2].to_string(),
            body: Vec::new(),
        }
    }
    pub fn get_method(&self) -> Method {
        return self.method.clone();
    }
    pub fn set_method(&mut self, method: Method) {
        self.method = method;
    }
    pub fn get_path(&self) -> String {
        return self.url.clone();
    }
    pub fn set_path(&mut self, url: String) {
        self.url = url;
    }
}

type Job = Box<dyn FnOnce() + Send + 'static>;

struct Worker {
    id: usize,
    thread: Option<JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Job>>>) -> Worker {
        let thread: JoinHandle<()> = spawn(move || loop {
            let job = match receiver.lock().expect("Failed to acquire mutex").recv() {
                Ok(job) => {
                    println!("Worker {} got a job; executing...", id);
                    job
                }
                Err(_) => {
                    println!("Worker {} disconnected; shutting down...", id);
                    break;
                }
            };
            job();
        });
        Worker {
            id,
            thread: Some(thread),
        }
    }
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: Option<mpsc::Sender<Job>>,
}

impl ThreadPool {
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers: Vec<Worker> = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }
        ThreadPool {
            workers,
            sender: Some(sender),
        }
    }
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender
            .as_ref()
            .unwrap()
            .send(job)
            .expect("Failed to send job!");
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        drop(self.sender.take());

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

pub fn write_response(mut stream: TcpStream, filepath: &Path, status_line: &str, has_body: bool) {
    let file_extension = match filepath.extension() {
        Some(file_extension) => file_extension,
        None => {
            eprintln!("Failed to get requested file's extension!");
            return;
        }
    };
    let contents = match file_extension.to_str() {
        Some("png") => fs::read(filepath).expect("Failed to read file!"),
        Some("ico") => fs::read(filepath).expect("Failed to read file!"),
        // Some("json") => CPUData::get_server_data(),
        _ => fs::read(filepath).expect("Failed to read file!"),
    };
    let length: usize = contents.len();
    let response: String = format!("{}\r\nContent-Length: {}\r\n\r\n", status_line, length);
    stream
        .write_all(response.as_bytes())
        .expect("Failed to write header!");
    if has_body {
        stream.write_all(&contents).expect("Failed to write body!");
    }
    stream.flush().expect("Failed to flush IO stream!");
}

pub fn handle_connection(mut stream: TcpStream, logger: Arc<Logger>, content_path: String) {
    let buf_reader: BufReader<&mut TcpStream> = BufReader::new(&mut stream);
    let http_request: Vec<String> = buf_reader
        .lines()
        .filter_map(|result| result.ok())
        .take_while(|line| !line.is_empty())
        .collect();
    println!("Request: {:#?}", http_request);
    logger.log(format!("Request: {:#?}", http_request).as_str());

    if http_request.len() < 1 {
        return;
    }

    let mut http_command = Request::new(&http_request[0]);

    println!("Commands: {:#?}", http_command);

    if http_command.get_path() == "/" {
        http_command.set_path(String::from("/index.xhtml"));
    }

    let mut filename: String = format!("{}{}", content_path, http_command.get_path());
    println!("Filename: {}", filename);
    let mut filepath: &Path = Path::new(filename.as_str());

    let mut needs_body = true;
    let mut status_line = "HTTP/1.1 404 NOT FOUND";
    // match Method::from(http_command[0]) {
    match http_command.get_method() {
        Method::Get => match Path::try_exists(filepath) {
            Ok(exists) => {
                status_line = if exists {
                    let status_line = "HTTP/1.1 200 OK";
                    logger.log(
                        format!(
                            "Item found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    status_line
                } else {
                    logger.log_error(
                        format!(
                            "Item not found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    filename = format!("{}/error.html", content_path);
                    filepath = Path::new(filename.as_str());
                    status_line
                };
            }
            Err(error) => {
                logger.log_error(error.to_string().as_str());
                eprintln!("{}", error);
                filename = format!("{}/error.html", content_path);
                filepath = Path::new(filename.as_str());
            }
        },
        Method::Head => match Path::try_exists(filepath) {
            Ok(exists) => {
                status_line = if exists {
                    let status_line = "HTTP/1.1 200 OK";
                    logger.log(
                        format!(
                            "Item found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    needs_body = false;
                    status_line
                } else {
                    logger.log_error(
                        format!(
                            "Item not found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    filename = format!("{}/error.html", content_path);
                    filepath = Path::new(filename.as_str());
                    status_line
                };
            }
            Err(error) => {
                logger.log_error(error.to_string().as_str());
                filename = format!("{}/error.html", content_path);
                filepath = Path::new(filename.as_str());
            }
        },
        Method::Post => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Put => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Delete => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Connect => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Options => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Trace => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Patch => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::BadRequest => {
            status_line = "HTTP/1.1 400 Bad Request";
            logger.log(
                format!(
                    "{} has no implementation!",
                    http_command.get_method().to_string()
                )
                .as_str(),
            );
            eprintln!(
                "{} has no implementation!",
                http_command.get_method().to_string()
            );
        }
    };
    write_response(stream, filepath, status_line, needs_body);
}

fn get_response(request: &Request) -> Response {
    Response::builder().status(200).build()
}

pub struct Webserver {
    pub logger: Arc<Logger>,
    pub config_file: File,
    pub content_path: String,
    ip: Ipv4Addr,
    port: u16,
    pub get_method: fn(&Request) -> Response,
}

impl Webserver {
    pub fn new(config_path: &str, content_path: &str) -> Webserver {
        // logger: Logger
        let file_location: &Path = Path::new(config_path);
        let config_file: File = OpenOptions::new()
            .append(true)
            .create(true)
            .read(true)
            .open(file_location)
            .expect("Failed to open file!");

        let logger = Logger::new(LOG_PATH);

        Webserver {
            logger,
            config_file,
            content_path: content_path.to_string(),
            ip: Ipv4Addr::new(127, 0, 0, 1),
            port: DEFAULT_PORT,
            get_method: get_response,
        }
    }

    pub fn start<A>(&self, ip: Option<A>, port: Option<u16>) -> TcpListener
    where
        A: Into<Ipv4Addr>,
    {
        // let buffered_reader = BufReader::new(self.config_file);

        let ip = match ip {
            Some(ip) => ip.into(),
            None => self.ip,
        };
        let port = match port {
            Some(port) => port,
            None => DEFAULT_PORT,
        };

        let mut test: String = String::new();
        let _ = self
            .config_file
            .try_clone()
            .expect("Failed to clone file!")
            .read_to_string(&mut test)
            .expect("Failed to read file!");

        self.logger.log(format!("config:\n{}", test).as_str());
        println!("config:\n{}", test);
        let args: Vec<String> = args().collect();
        let mut address: String = String::from(format!("{}:{}", ip, port));
        if args.len() > 1 {
            address = String::from(format!("{}:{}", args[1], DEFAULT_PORT));
        }
        println!("path argument: {}", args[0]);

        println!("Starting webserver on: {}", address);
        self.logger.log(address.as_str());
        let listener: TcpListener = match TcpListener::bind(address) {
            Ok(socket) => {
                println!("Bind successful!");
                socket
            }
            Err(error) => {
                self.logger.log_error(error.to_string().as_str());
                panic!("Error: {}", error);
            }
        };
        return listener;

        // let pool: ThreadPool = ThreadPool::new(5);
        // for stream in listener.incoming() {
        //     let logger_copy = Arc::clone(&self.logger);
        //     let content_path = self.content_path.clone();
        //     match stream {
        //         Ok(stream) => {
        //             pool.execute(|| {
        //                 // handler(stream, logger_copy);
        //                 handle_connection(stream, logger_copy, content_path);
        //             });
        //         }
        //         Err(error) => {
        //             self.logger.log_error(format!("{error}").as_str());
        //             // panic!("Error: {}", e);
        //         }
        //     }
        // }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let test: String = match Method::from("get") {
            Method::Get => String::from("Get"),
            Method::Head => String::from("Head"),
            Method::Post => String::from("Post"),
            Method::Put => String::from("Put"),
            Method::Delete => String::from("Delete"),
            Method::Connect => String::from("Connect"),
            Method::Options => String::from("Options"),
            Method::Trace => String::from("Trace"),
            Method::Patch => String::from("Patch"),
            Method::BadRequest => String::from("BadRequest"),
        };
        dbg!(test);
        dbg!(Method::from("head"));
        assert_eq!(Method::Get, Method::from("get"));
        debug_assert_eq!(Method::Get, Method::from("get"));
    }

    #[test]
    fn webserver_builder_test() {
        let webserver = WebServer::builder().ip("127.0.0.1").port(80).build();
    }

    #[test]
    fn response_builder_test() {
        let response = Response::builder().status(404).build();
    }
}
